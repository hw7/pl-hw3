(* Semyon Gufman 336455308 gu.semyon@campus.technion.ac.il
 * Eitan Tugendhaft 307955823 eitan.t@campus.technion.ac.il *)

(* Section 2.1 *)

(* Q1 *)

(* In case [] given as an argument *)
exception Undefined;

(* Helper functions *)
fun numberOfOccurrences [] (str : string) = 0
    | numberOfOccurrences (x :: xs) str =
        if str = x then 1 + (numberOfOccurrences xs str) else (numberOfOccurrences xs str);

fun max (x, y) = if x > y then x else y;

fun maxInList [] = 0
  | maxInList (x :: xs) = let val y = (maxInList xs) in max (x, y) end;

fun numberOfOccurrencesList [] = raise Undefined
    | numberOfOccurrencesList list = List.map (numberOfOccurrences list) list;

fun commonAux list [] _ = raise Undefined
    | commonAux list _ [] = raise Undefined
    | commonAux list (x :: xs) (y :: ys) = 
        if x = maxInList list then y else commonAux list xs ys;

(* Main function *)
fun common [] = raise Undefined
    | common list = commonAux (numberOfOccurrencesList list) (numberOfOccurrencesList list) list; 


(* Q2 *)

(* Helper functions *)
fun revertList [] = []
    | revertList (x :: xs) = (revertList xs) @ [x];

fun sequenceAux [] maxSequence currentSequence =
        if List.length maxSequence < List.length currentSequence then 
            revertList currentSequence 
        else 
            revertList maxSequence
    | sequenceAux (x :: xs) maxSequence [] = sequenceAux xs maxSequence (x :: [])
    | sequenceAux (x :: xs) maxSequence (y :: ys) = 
        if x <> (y + 1) then 
            if List.length maxSequence < List.length (y :: ys) then
                sequenceAux xs (y :: ys) []
            else
                sequenceAux xs maxSequence []
        else sequenceAux xs maxSequence (x :: (y :: ys));

(* Main function *)
fun sequence [] = raise Undefined
    | sequence list = sequenceAux list [] [];


(* Q3 *)

fun sort list = foldr (fn (x,lst)=> List.filter (fn a => a < x) lst @ [x] @ List.filter (fn a => a >= x) lst ) [] list;
