(* Semyon Gufman 336455308 gu.semyon@campus.technion.ac.il
 * Eitan Tugendhaft 307955823 eitan.t@campus.technion.ac.il *)

(* Section 2.2 *)

(* Q1 *)
fun are_nighbours ((l1, u1, r1, d1), (l2, u2, r2, d2)) = 
    l1 = r2 orelse l2 = r1 orelse u1 = d2 orelse u2 = d1;  

(* Q2 *)

(* Helper functions *)
fun transp ([] :: _) = []
    | transp rows = (map hd rows) :: transp (map tl rows)

fun isValidRow [] = true
    | isValidRow (x :: []) = true
    | isValidRow ((_, _, r1, _) :: ((l2, u2, r2, d2) :: xs)) =
        (r1 = l2) andalso (isValidRow ((l2, u2, r2, d2) :: xs));

fun isValidCol [] = true
    | isValidCol (x :: []) = true
    | isValidCol ((_, _, _, d1) :: ((l2, u2, r2, d2) :: xs)) =
        (d1 = u2) andalso (isValidCol ((l2, u2, r2, d2) :: xs));

(* Main function *)
fun is_valid_maze [] = false
    | is_valid_maze maze = if List.all isValidRow maze then List.all isValidCol (transp maze) else false

(* Q3 *)
datatype moves = LEFT | RIGHT | UP | DOWN;

(* Helper functions *)
fun getMatrixElement matrix (x, y) =
        List.hd (List.drop ((List.hd (List.drop (matrix, x))), y));

fun canGo (l, u, r, d) move =
    case move of
      LEFT => l = 1
    | RIGHT => r = 1
    | UP => u = 1
    | DOWN => d = 1   

fun outOfMatrix matrix (x, y) =
    x >= List.length matrix orelse y >= List.length (hd matrix)
        orelse x < 0 orelse y < 0;

fun validMazeSolutionAux maze position [] = outOfMatrix maze position
    | validMazeSolutionAux maze (x, y) (move :: movesLeft) =
        if outOfMatrix maze (x, y) then false else
            if canGo (getMatrixElement maze (x, y)) move then
                case move of
                  LEFT => validMazeSolutionAux maze (x, y - 1) movesLeft
                | RIGHT => validMazeSolutionAux maze (x, y + 1) movesLeft
                | UP => validMazeSolutionAux maze (x - 1, y) movesLeft
                | DOWN => validMazeSolutionAux maze (x + 1, y) movesLeft
            else false;

(* Main function *)
fun valid_maze_solution (maze, start, path) = if (is_valid_maze maze) = false then false else 
    if is_valid_maze maze  then
        validMazeSolutionAux maze start path
    else false;

(* Q4 *)

(* Helper functions *)
fun inMatrix matrix (x, y) = x < (List.length matrix) andalso y < (List.length (List.hd matrix));

fun cornerExit maze =
    let
        val lastRowIndex = (List.length maze) - 1
        val lastColIndex = (List.length (List.hd maze)) - 1
        val (tlL, tlU, tlR, tlD) = getMatrixElement maze (0, 0)
        val (trL, trU, trR, trD) = getMatrixElement maze (0, lastColIndex)
        val (blL, blU, blR, blD) = getMatrixElement maze (lastRowIndex, 0)
        val (brL, brU, brR, brD) = getMatrixElement maze (lastRowIndex, lastColIndex)
        val tlExit = tlL = 1 andalso tlU = 1
        val trExit = trR = 1 andalso trU = 1
        val blExit = blL = 1 andalso blD = 1
        val brExit = brR = 1 andalso brD = 1
    in
       tlExit orelse trExit orelse blExit orelse brExit 
    end;

fun topEntrances maze =
    let
        val topRow = List.hd maze
        fun topEntrancesAux [] _ = []
            | topEntrancesAux (x :: xs) index =
                if canGo x UP then (0, index) :: (topEntrancesAux xs (index + 1))
                else topEntrancesAux xs (index + 1)
    in
        topEntrancesAux topRow 0
    end;

fun bottomEntrances maze =
    let
        val bottomRowIndex = (List.length maze) - 1
        val bottomRow = List.hd (List.drop (maze, bottomRowIndex))
        fun bottomEntrancesAux [] _ = []
            | bottomEntrancesAux (x :: xs) index =
                if canGo x DOWN then (bottomRowIndex, index) :: (bottomEntrancesAux xs (index + 1))
                else bottomEntrancesAux xs (index + 1)
    in
        bottomEntrancesAux bottomRow 0
    end;

fun leftEntrances maze =
    let
        val leftCol = List.hd (transp maze)
        fun leftEntrancesAux [] _ = []
            | leftEntrancesAux (x :: xs) index =
                if canGo x LEFT then (index, 0) :: (leftEntrancesAux xs (index + 1))
                else leftEntrancesAux xs (index + 1)
    in
        leftEntrancesAux leftCol 0
    end;

fun rightEntrances maze =
    let
        val transpMaze = transp maze
        val rightColIndex = (List.length transpMaze) - 1
        val rightCol = List.hd (List.drop (transpMaze, rightColIndex))
        fun rightEntrancesAux [] _ = []
            | rightEntrancesAux (x :: xs) index =
                if canGo x RIGHT then (index, rightColIndex) :: (rightEntrancesAux xs (index + 1))
                else rightEntrancesAux xs (index + 1)
    in
        rightEntrancesAux rightCol 0
    end;

fun closeDoor maze (x, y) move =
    let
        val (l, u, r, d) = getMatrixElement maze (x, y)
        val relevantRow = List.hd (List.drop (maze, x))
        val rowsBefore = List.take (maze, x)
        val rowsAfter = List.drop (maze, x + 1)
        val roomsBefore = List.take (relevantRow, y)
        val roomsAfter = List.drop (relevantRow, y + 1)
    in
        case move of
          LEFT => rowsBefore @ [roomsBefore @ [(0, u, r, d)] @ roomsAfter] @ rowsAfter
        | RIGHT => rowsBefore @ [roomsBefore @ [(l, u, 0, d)] @ roomsAfter] @ rowsAfter
        | UP => rowsBefore @ [roomsBefore @ [(l, 0, r, d)] @ roomsAfter] @ rowsAfter
        | DOWN => rowsBefore @ [roomsBefore @ [(l, u, r, 0)] @ roomsAfter] @ rowsAfter
    end;

fun searchSolution maze currentPlace =
    let
        val (x, y) = currentPlace
        val currentRoom = getMatrixElement maze currentPlace
        val upPlace = (x - 1, y)
        val leftPlace = (x, y - 1)
        val rightPlace = (x, y + 1)
        val downPlace = (x + 1, y)
    in
        if ((canGo currentRoom UP) andalso (outOfMatrix maze upPlace)) orelse
        ((canGo currentRoom LEFT) andalso (outOfMatrix maze leftPlace)) orelse
        ((canGo currentRoom RIGHT) andalso (outOfMatrix maze rightPlace)) orelse
        ((canGo currentRoom DOWN) andalso (outOfMatrix maze downPlace)) then true else
            ((canGo currentRoom UP) andalso (searchSolution (closeDoor (closeDoor maze currentPlace UP) upPlace DOWN) upPlace)) orelse
            ((canGo currentRoom RIGHT) andalso (searchSolution (closeDoor (closeDoor maze currentPlace RIGHT) rightPlace LEFT) rightPlace)) orelse
            ((canGo currentRoom LEFT) andalso (searchSolution (closeDoor (closeDoor maze currentPlace LEFT) leftPlace RIGHT) leftPlace)) orelse
            ((canGo currentRoom DOWN) andalso (searchSolution (closeDoor (closeDoor maze currentPlace DOWN) downPlace UP) downPlace))
    end;

fun topEntranceSol maze [] = false
    | topEntranceSol maze (x :: xs) =
        searchSolution (closeDoor maze x UP) x orelse topEntranceSol maze xs;

fun leftEntranceSol maze [] = false
    | leftEntranceSol maze (x :: xs) =
        searchSolution (closeDoor maze x LEFT) x orelse leftEntranceSol maze xs;

fun bottomEntranceSol maze [] = false
    | bottomEntranceSol maze (x :: xs) =
        searchSolution (closeDoor maze x DOWN) x orelse bottomEntranceSol maze xs;

fun rightEntranceSol maze [] = false
    | rightEntranceSol maze (x :: xs) =
        searchSolution (closeDoor maze x RIGHT) x orelse rightEntranceSol maze xs;

(* Main function *)
fun exit_maze maze =
    if is_valid_maze maze then
        cornerExit maze orelse
        (topEntranceSol maze (topEntrances maze)) orelse
        (leftEntranceSol maze (leftEntrances maze)) orelse
        (rightEntranceSol maze (rightEntrances maze)) orelse
        (bottomEntranceSol maze (bottomEntrances maze))
    else false

(* Q5 *)

(* Helper functions *)
fun createEmptyRow [] = []
    | createEmptyRow (x :: xs) = 0 :: (createEmptyRow xs);

fun createEmptyMatrix [] = []
    | createEmptyMatrix (x :: xs) = (createEmptyRow x) :: (createEmptyMatrix xs);

fun putOneInMatrix matrix (x, y) =
    let
        val relevantRow = List.hd (List.drop (matrix, x))
        val rowsBefore = List.take (matrix, x)
        val rowsAfter = List.drop (matrix, x + 1)
        val elemsBefore = List.take (relevantRow, y)
        val elemsAfter = List.drop (relevantRow, y + 1)
    in
        rowsBefore @ [elemsBefore @ [1] @ elemsAfter] @ rowsAfter
    end;

fun searchPathAux maze currentPlace moves =
    let
        val (x, y) = currentPlace
        val currentRoom = getMatrixElement maze currentPlace
        val upPlace = (x - 1, y)
        val leftPlace = (x, y - 1)
        val rightPlace = (x, y + 1)
        val downPlace = (x + 1, y)
        val upMove = ((canGo currentRoom UP) andalso (outOfMatrix maze upPlace))
        val leftMove = ((canGo currentRoom LEFT) andalso (outOfMatrix maze leftPlace))
        val rightMove = ((canGo currentRoom RIGHT) andalso (outOfMatrix maze rightPlace))
        val downMove = ((canGo currentRoom DOWN) andalso (outOfMatrix maze downPlace))
        val upPath = if (canGo currentRoom UP) then (searchPath (closeDoor (closeDoor maze currentPlace UP) upPlace DOWN) upPlace (moves @ [UP])) else []
        val rightPath = if (canGo currentRoom RIGHT) then (searchPath (closeDoor (closeDoor maze currentPlace RIGHT) rightPlace LEFT) rightPlace (moves @ [RIGHT])) else []
        val leftPath = if (canGo currentRoom LEFT) then (searchPath (closeDoor (closeDoor maze currentPlace LEFT) leftPlace RIGHT) leftPlace (moves @ [LEFT])) else []
        val downPath = if (canGo currentRoom DOWN) then (searchPath (closeDoor (closeDoor maze currentPlace DOWN) downPlace UP) downPlace (moves @ [DOWN])) else []
    in
        if leftPath <> [] then leftPath else if upPath <> [] then upPath else if rightPath <> [] then rightPath else if downPath <> [] then downPath else []
    end
and searchPath maze currentPlace moves =
    let
        val (x, y) = currentPlace
        val currentRoom = getMatrixElement maze currentPlace
        val upPlace = (x - 1, y)
        val leftPlace = (x, y - 1)
        val rightPlace = (x, y + 1)
        val downPlace = (x + 1, y)
        val upMove = ((canGo currentRoom UP) andalso (outOfMatrix maze upPlace))
        val leftMove = ((canGo currentRoom LEFT) andalso (outOfMatrix maze leftPlace))
        val rightMove = ((canGo currentRoom RIGHT) andalso (outOfMatrix maze rightPlace))
        val downMove = ((canGo currentRoom DOWN) andalso (outOfMatrix maze downPlace))
    in
        case (leftMove, upMove, rightMove, downMove) of
          (true, _, _, _) => moves @ [LEFT]
        | (_, true, _, _) => moves @ [UP]
        | (_, _, true, _) => moves @ [RIGHT]
        | (_, _, _, true) => moves @ [DOWN]
        | (_, _, _, _) => searchPathAux maze currentPlace moves
    end;

fun topEntrancePath maze [] = ([], (~1, ~1))
    | topEntrancePath maze (x :: xs) =
        let
            val searchPathX = searchPath (closeDoor maze x UP) x []
        in
            if searchPathX <> [] then (searchPathX, x) else topEntrancePath maze xs
        end;

fun leftEntrancePath maze [] = ([], (~1, ~1))
    | leftEntrancePath maze (x :: xs) =
        let
            val searchPathX = searchPath (closeDoor maze x LEFT) x []
        in
            if searchPathX <> [] then (searchPathX, x) else leftEntrancePath maze xs
        end;

fun bottomEntrancePath maze [] = ([], (~1, ~1))
    | bottomEntrancePath maze (x :: xs) =
        let
            val searchPathX = searchPath (closeDoor maze x DOWN) x []
        in
            if searchPathX <> [] then (searchPathX, x) else bottomEntrancePath maze xs
        end;

fun rightEntrancePath maze [] = ([], (~1, ~1))
    | rightEntrancePath maze (x :: xs) =
        let
            val searchPathX = searchPath (closeDoor maze x RIGHT) x []
        in
            if searchPathX <> [] then (searchPathX, x) else rightEntrancePath maze xs
        end;

fun getExitPath maze =
    let
        val topEntranceExit = (topEntrancePath maze (topEntrances maze))
        val leftEntranceExit = (leftEntrancePath maze (leftEntrances maze))
        val rightEntranceExit = (rightEntrancePath maze (rightEntrances maze))
        val bottomEntranceExit = (bottomEntrancePath maze (bottomEntrances maze))
    in
        if topEntranceExit <> ([], (~1, ~1)) then topEntranceExit else if leftEntranceExit <> ([], (~1, ~1)) then leftEntranceExit else
            if rightEntranceExit <> ([], (~1, ~1)) then rightEntranceExit else bottomEntranceExit
    end;

fun followPath maze current [] = true
    | followPath maze current (move :: xs) =
        let
            val (x, y) = current
        in
            if exit_maze (closeDoor maze current move) then false else
            case move of
              UP => followPath maze (x - 1, y) xs
            | LEFT => followPath maze (x, y - 1) xs
            | RIGHT => followPath maze (x, y + 1) xs
            | DOWN => followPath maze (x + 1, y) xs    
        end;

fun one_path_maze maze =
    let
        val (moves, start) = getExitPath maze
    in
        if (is_valid_maze maze) = false then false else
            if moves = [] then false else followPath maze start moves
    end;
    

